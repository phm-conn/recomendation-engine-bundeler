export type ArangoAuthentication = {
  username: string;
  password: string;
  jwt?: undefined | null | false;
} | {
  username?: undefined | null | false;
  password?: undefined | null | false;
  jwt: string;
};

export type Domains = string;
export type StoryID = string;
export type UserID = string;

export interface MicroUID {
  id: number;
  domain: Domains;
}
export interface MiniUID extends MicroUID {
  name: string;
}

export interface Story extends MiniUID {
  lastScrape?: number;
  wordcount: number;
  leadcount?: number;
  datesubmit: number;
  dateupdate: number;
  chapters: number;
  statusid: number;
  blurb: string;
  fandoms: Array<string>;
  tags: Array<string>;
}

export interface FullStory extends Story {
  _key: string;
  _id: string;
  _rev: string;
}

export interface User extends MiniUID {
  lastScrape?: number;
}

export type Review = { "time": number; "review": string };
export type ReviewGroup = Record<string, Review>;

export type ReviewPointer = {
  _from: StoryID;
  _to: UserID;
  reviews: ReviewGroup;
};

export type Favorite = {
  _from: UserID;
  _to: StoryID;
};

export type Author = {
  _from: UserID;
  _to: StoryID;
};

export interface UserDump extends User {
  authored: Array<MiniUID>;
  favedBooks: Array<MiniUID>;
}
export interface StoryDump extends MicroUID {
  details: FullStory;
  authors: Array<User>;
  faved: Array<User>;
}
export interface LeadDump extends MicroUID {
  leadcount: number;
  reviewed: Array<MicroUID>;
}
